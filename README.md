# Nordic Digital AS products XML example 
  
Nodes and values marked with ** are optional. If you have translations, please include them.   
stock_quantity - Current stock availability.  
price - Price you sell to us.

**There is two XML versions below.**

### XML example without optional values and translations:  
__ 
```xml
<?xml version="1.0" encoding="UTF-8"?>
<products>
    <product>
        <product_code>LW8CA7DC</product_code>
        <product_id>123233</product_id>
        <manufacturer_code>LW8CA7DCMAN</manufacturer_code>
        <ean>3292939239932</ean>
        <vendor>BIC</vendor>
        <name>BIC® Special Edition® Casino Series Lighters</name>
        <short_description>Capture the excitement of slot machines, card tables, dice and roulette</short_description>
        <description>8-Pack of BIC Special Edition Casino Series Lighters with a holographic foil
Child-resistant, safe and reliable, 100% quality inspected
Up to 2 times the lights vs. the next full size leading brand
Every BIC® lighter undergoes more than 50 separate, automatic quality checks during the manufacturing process
Made in the USA
Designs may vary</description>
        <category>Lighters</category>
        <stock_quantity>55</stock_quantity>
        <price>14.30</price>
    </product>
</products>
```
  
### Full XML example with translations and optional values:  
__ 
```xml
<?xml version="1.0" encoding="UTF-8"?>
<products>
    <product>
        <product_code>LW8CA7DC</product_code>
        <product_id>123233</product_id>
        <manufacturer_code>LW8CA7DCMAN</manufacturer_code>
        <ean>3292939239932</ean>
        <vendor>BIC</vendor>
        <names>
            <name lang="en">BIC® Special Edition® Casino Series Lighters</name>
            <name lang="et">BIC® Special Edition® Casino Series Lighters</name>**
        </names>
        <short_descriptions>**
            <name lang="en">Capture the excitement of slot machines, card tables, dice and roulette</name>
            <name lang="et">Capture the excitement of slot machines, card tables, dice and roulette</name>**
        </short_descriptions>
        <descriptions>
            <name lang="en">8-Pack of BIC Special Edition Casino Series Lighters with a holographic foil
Child-resistant, safe and reliable, 100% quality inspected
Up to 2 times the lights vs. the next full size leading brand
Every BIC® lighter undergoes more than 50 separate, automatic quality checks during the manufacturing process
Made in the USA
Designs may vary</name>
            <name lang="et">8-Pack of BIC Special Edition Casino Series Lighters with a holographic foil
Child-resistant, safe and reliable, 100% quality inspected
Up to 2 times the lights vs. the next full size leading brand
Every BIC® lighter undergoes more than 50 separate, automatic quality checks during the manufacturing process
Made in the USA
Designs may varyBIC® Special Edition® Casino Series Lighters</name>**
        </descriptions>
        <category>Lighters</category>
        <category_id>1233</category_id>**
        <stock_quantity>1</stock_quantity>
        <rrp>16.99</rrp>**
        <price>14.30</price>
        <atrributes>**
            <attribute>
				<name lang="en">Color</name>
                <value lang="en">Yellow</value>
                <name lang="et">Värv</name>**
                <value lang="et">Kollane</value>**
			</attribute>
			<attribute>
                <name lang="en">Child-resistant</name>
                <value lang="en">Safe</value>
                <name lang="et">Child-resistant</name>**
                <value lang="et">Safe</value>**
            </attribute>
        </attributes>
    </product>
</products>
```
